# Archi As Code

## Quick start
to convert your Yaml files to HTML and SVG documents, simply run the following commands.
`$AAC_DIR` représente le chemin dans lequel l'appli a été déposée 


    $> cd $AAC_DIR 
    $> virtualenv -p /usr/bin/python3 venv
    $> source venv/bin/activate
    $> pip install -r requirements.txt
    $> python3 ./src/yaml2diagram.py -i a_yaml_file -d ouput_directory
    
## Define your 1st diagram

The yaml file is expected to follow the following structure : 

    schema:
      meta:
        application: application title
        project: project name
        C: Confidentiality requirement
        I: Integrity requirement
        D: Availability requirement
        T: Traces requirement
        ICP: availability requirement
        RTO: Recovery Time Objective
        RPO: Recovery point objective (max data loss)
        DRP: Disaster recovery strategy 
        status: draft | validated
        technical-architect: architect's name
        security-architect: architect's name
        
      dependencies:
        - id:   dep id
          name: dep name
          role: NS | Messaging | Monitoring | PA | 
          description: dep description
        ...
        
      contents:
        - type: intranet | internet | internetF | internetP | internetA | internetPA | internetD | intranetP ...
          name: zone name
          services:
          - id:   service id
            name: service name
            role: C | P | A | D | PA | VPN | LB
            description: service description
          nodes:
          - id:   service id
            name: service name
            role: C | P | A | D | PA | VPN | LB |  
            description: service description
            system: System (Linux, WIndows, ...)
            nodes: number of nodes
            infra: infrastructure type (SaaS, PaaS, IaaS, Bare Metal)
            topology: kind of replication / redundancy
            active: HA mode (active + active, Active + passive, single active, ...)
            components:
            - list of components
          ...
         flows: 
           - origin: id of source node/service
             dest:   id of target node/service
             proto:  L7 protocole name
             cipher: TLS, ssh, PGP, None, ...
             type:   web, database, ssh, 
             port:   TCP/UDP port used
           ...
           
More to come in a much more comprehensive documentation.