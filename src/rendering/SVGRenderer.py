import jinja2

from constants import TEMPLATES_PATH
from rendering.Renderer import Renderer


class SVGRenderer(Renderer):

    def render(self) -> str:
        self.logger.debug("Starting SVG rendering")
        self.diagram.compute_layout()
        template_loader = jinja2.FileSystemLoader(searchpath=TEMPLATES_PATH)
        template_env = jinja2.Environment(loader=template_loader, autoescape=True)
        template = template_env.get_template("diagram.svg")
        svg = template.render(data=self.diagram, basename=self.basename)
        return svg
