import json

from .Renderer import Renderer


class JSONRenderer(Renderer):

    def render(self) -> str:
        return json.dumps(self.diagram, default=lambda o: o.__dict__, sort_keys=True, indent=4)
