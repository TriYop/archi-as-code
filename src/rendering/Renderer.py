import logging

from domain import Diagram


class Renderer():
    logger = logging.getLogger("Renderer")

    """Render a diagram's information to a particular format."""

    def __init__(self, diagram: Diagram, basename: str):
        """
        Args:
            diagram (Diagram):
        """
        self.basename = basename
        self.diagram = diagram

    def render(self) -> str:
        """Render the diagram and return a String containing it."""
        pass
