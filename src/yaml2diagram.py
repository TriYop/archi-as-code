#!/bin/env python3
# coding = utf8

import json
import logging
from argparse import ArgumentParser
from os import makedirs
from os.path import basename, exists, isdir

from domain.Diagram import Diagram
from rendering.HTMLRenderer import HTMLRenderer
from rendering.SVGRenderer import SVGRenderer


class MyEncoder(json.JSONEncoder):
    """Utility class for outputing JSON content"""

    def default(self, o):
        return o.__dict__


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG,
                        format="%(asctime)-15s - %(levelname)-7s - %(filename)-20s - Line %(lineno)-04d - %(funcName)-20s - %(message)s")
    logger = logging.getLogger("yaml2diagram")

    arp = ArgumentParser(description="Converts YAML network description to svg diagramm and HTML document")
    arp.add_argument("-i", dest="schema", help="sets input file to convert", nargs='*')
    arp.add_argument("-d", dest="dest", help="sets output directory", nargs=1)
    args = arp.parse_args()
    if args.schema is None:
        arp.print_help()
        exit(1)

    # schemas = ["../riskandme-prod.archi"]
    for schema_file in args.schema:
        if schema_file.endswith(".yaml"):
            schema_file = schema_file.replace(".yaml", "")
        filename = basename(schema_file)
        source = ""
        with open(f"{schema_file}.yaml", 'r') as stream:
            source = stream.read()
        schema = Diagram(source)
        if args.dest is None:
            dest = "."
        else:
            dest = args.dest[0]

            if not (exists(dest) or isdir(dest)):
                makedirs(dest)
        with open(f"{dest}/{schema_file}.svg", 'w') as schema_svg:
            schema_svg.write(SVGRenderer(schema, filename).render())
        with open(f"{dest}/{schema_file}.html", 'w') as schema_html:
            schema_html.write(HTMLRenderer(schema, filename).render())
