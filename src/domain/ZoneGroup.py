import logging

from constants import ZONES_OPTIONS, INTERZONE_SPACE_Y, INTERNODE_SPACE_X, ZONE_OFFSET_X, ZONE_OFFSET_Y, INTERZONE_SPACE_X, INTERNODE_SPACE_Y

class Zonegroup(object):

    logger = logging.getLogger("ZoneGroup")

    def __init__(self, id):
        self.id = id
        self.zones = []
        self.current_offset = 0
        self.left = 0
        self.top = 0
        self.width = 0
        self.height = 0
        self.logger.debug(f"Created zonegroup {self.id}")

    def append_zone(self, zone):
        self.zones.append(zone)

    def layout_content(self):
        """
        Layout of zone contained objects (nodes and services) in a way that flows may pass inbetween;
        :return:
        """
        offset_x = 0
        offset_y = 0
        share_mode = ZONES_OPTIONS[self.id]['share']
        for zone in self.zones:
            (w, h) = zone.layout_content(offset_x, offset_y)
            if share_mode == 'width':
                offset_x += w + INTERNODE_SPACE_X
            else:
                offset_y += h + INTERZONE_SPACE_Y

        if share_mode == 'width':
            self.width = sum([zone.width for zone in self.zones]) + 2 * ZONE_OFFSET_X + INTERZONE_SPACE_X * (
                    len(self.zones) - 1)
            self.height = max([zone.height for zone in self.zones])
        else:
            self.width = max([zone.width for zone in self.zones])
            self.height = sum([zone.height for zone in self.zones]) + 2 * ZONE_OFFSET_Y + INTERZONE_SPACE_Y * (
                    len(self.zones) - 1)

