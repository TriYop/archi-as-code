import logging

from domain.Node import Node
from constants import *


class Zone(object):
    logger = logging.getLogger("Zone")

    def __init__(self, id, contents, parent_diagram):
        """
        Args:
            id:
            contents:
            parent_diagram:
        """
        self.id = id
        self.name = contents.get('name', '')
        self.type = contents.get('type', '')
        self.pds = contents.get('PDS', 'intranet')
        self.description = contents.get('description', '')
        self.in_offset = 4
        self.out_offset = 4
        self.left = 0
        self.top = 0
        self.width = 0
        self.height = 0
        self.diagram = parent_diagram

        # Create nodes
        self.nodes = []
        self.services = []
        self.all_nodes = []
        if 'nodes' in contents:
            self.nodes = [Node(node_id, node, self) for node_id, node in enumerate(contents['nodes'])]
        if 'services' in contents:
            self.services = [Node(node_id, node, self) for node_id, node in enumerate(contents['services'])]
        self.nodes_count = len(self.nodes) + len(self.services)
        self.in_lane = None
        self.out_lane = None
        self.logger.debug("Created zone " + self.id)

    def set_lanes(self, inlane=None, outlane=None):
        """
        Args:
            inlane:
            outlane:
        """
        if inlane:
            self.in_lane = inlane
        if outlane:
            self.out_lane = outlane

    def layout_content(self, offset_x, offset_y):
        """Layout zone contents

        Args:
            offset_x:
            offset_y:
        """
        self.all_nodes.extend(self.services)
        self.all_nodes.extend(self.nodes)

        if ZONES_OPTIONS[self.type]['height'] > 0:
            ny = ZONES_OPTIONS[self.type]['height']
            self.height = (NODES_GRID_Y + INTERZONE_SPACE_Y) * (
                ny) - INTERZONE_SPACE_Y + 2 * ZONE_PADDING + ZONE_OFFSET_Y
        else:
            ny = self.nodes_count
            self.height = (
                                  NODES_GRID_Y + INTERNODE_SPACE_Y) * self.nodes_count + INTERNODE_SPACE_Y + ZONE_OFFSET_Y + 2 * ZONE_PADDING - INTERZONE_SPACE_Y

        columns = (self.nodes_count / ny)

        if (self.nodes_count % ny) > 0:
            columns += 1

        self.width = ZONE_OFFSET_X + (INTERNODE_SPACE_X + NODES_GRID_X) * columns - INTERNODE_SPACE_X + 2 * ZONE_PADDING

        self.left = offset_x
        self.top = offset_y

        idx = 0

        for svc in self.all_nodes:
            svc.left = ZONE_OFFSET_X + ZONE_PADDING + (NODES_GRID_X + INTERNODE_SPACE_X) * int(idx / ny)
            svc.top = ZONE_OFFSET_Y + ZONE_PADDING + (NODES_GRID_Y + INTERNODE_SPACE_Y) * (idx % ny)
            svc.offset_x = self.left
            svc.offset_y = self.top
            idx += 1

        return (self.width, self.height)

    def get_in_offset(self, service):
        """
        Args:
            service:
        """
        self.in_offset += 1
        return self.in_offset

    def get_out_offset(self, service):
        """
        Args:
            service:
        """
        self.out_offset += 1
        return self.out_offset
