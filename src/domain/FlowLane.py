import logging

class FlowLane(object):
    logger =  logging.getLogger("FlowLane")

    def __init__(self):
        self.top = 0
        self.bottom = 0
        self.left = 0
        self.right = 0
        self.offset = 0
        
        self.logger.debug("Created flow lane " + self.id)

