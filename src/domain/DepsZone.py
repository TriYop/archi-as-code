import logging

from domain.Node import Node
from domain.Zone import Zone


class DepsZone(Zone):
    logger = logging.getLogger("DepsZone")

    def __init__(self, contents, parent_diagram):
        super(DepsZone, self).__init__("svc", contents, parent_diagram)
        self.name = contents.get("name", "Services externes")
        self.description = "Services tiers hors du scope de l'application"
        self.type = "DEP"
        self.nodes = []
        self.all_nodes = []
        self.services = [Node(f"X{node_id}", node, self) for node_id, node in enumerate(contents.get("services", []))]
        self.nodes_count = len(self.nodes) + len(self.services)
        self.diagram = parent_diagram

        self.offset_in = {}
        self.offset_out = {}
        self.logger.debug(f"Created depszone {self.id}")

    def get_in_offset(self, service):
        if not service.id in self.offset_in:
            self.offset_in[service.id] = 1
        self.offset_in[service.id] += 1
        return self.offset_in[service.id]

    def get_out_offset(self, service):
        if not service.id in self.offset_out:
            self.offset_out[service.id] = 0
        self.offset_out[service.id] += 1
        return self.offset_out[service.id]
