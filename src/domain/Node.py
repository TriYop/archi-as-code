import logging

from constants import NODES_GRID_X, NODES_GRID_Y, DEFAULT_TOPOLOGY, DEFAULT_ACTIVE, PATH_OFFSET


class Node(object):
    logger = logging.getLogger("Node")

    def __init__(self, node_id: str, node: dict, zone):

        self.uid = node_id
        self.zone = zone
        self.id = node.get("id", node_id)
        self.name = node.get("name", "")
        self.description = node.get("description", "")
        self.role = node.get("role", "")
        self.topology = node.get("topology", DEFAULT_TOPOLOGY)
        self.active = node.get("active", DEFAULT_ACTIVE)
        self.infra = node.get("infra", "cloud")
        self.status = node.get("status", "new")
        self.system = node.get("system", "unknown")
        self.components = node.get("components", [])

        # Layout
        self.width = NODES_GRID_X
        self.height = NODES_GRID_Y
        self.left = 0
        self.top = 0
        self.offset_x = 0
        self.offset_y = 0
        self.offset_in = 0
        self.offset_out = 0
        self.in_connectors = {}
        self.out_connectors = {}

        self.logger.debug(f"Created node {self.id}")

    def get_in_connector(self, protocol):
        if protocol not in self.in_connectors:
            self.in_connectors[protocol] = self.get_next_in_connector()
        return self.in_connectors[protocol]

    def get_out_connector(self, protocol):
        if protocol not in self.out_connectors:
            self.out_connectors[protocol] = self.get_next_out_connector()
        return self.out_connectors[protocol]

    def get_next_in_connector(self):
        # FIXME: append global Y offset based on node position
        offset = self.zone.get_in_offset(self)

        return (self.left + PATH_OFFSET * (1 + len(self.in_connectors)) + self.offset_x + self.offset_in,
                self.top + self.offset_y,
                PATH_OFFSET / 5 * (offset * +3))

    def get_next_out_connector(self):
        # FIXME: append global Y offset based on node position
        offset = self.zone.get_out_offset(self)
        return (self.left + PATH_OFFSET * (1.5 + len(self.out_connectors)) + self.offset_x + self.offset_out,
                self.top + self.offset_y + self.height,
                PATH_OFFSET / 5 * (offset * +3))

    def get_in_lane(self):
        return self.zone.in_lane

    def get_out_lane(self):
        return self.zone.out_lane
