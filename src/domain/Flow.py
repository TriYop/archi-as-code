import logging
from constants import ZONE_PADDING, NODES_GRID_X, NODES_GRID_Y, PATH_TRUNK_LEFT, PATH_OFFSET


class Flow(object):
    logger = logging.getLogger("Flow")

    def __init__(self, id, contents, parent_diagram):
        """
        Args:
            id:
            contents:
            parent_diagram:
        """
        self.id = id
        self.type = contents.get('type', 'TCP')
        self.port = contents.get('port', '')
        self.proto = contents.get('proto', 'unknown')
        self.cipher = contents.get('cipher', 'NONE')
        self.origin = contents.get('origin', '*')
        self.dest = contents.get('dest', '*')
        self.description = contents.get('description', '')
        self.layout = ""
        self.dest_x = 0
        self.dest_y = 0
        self.diagram = parent_diagram
        self.logger.debug("Created flow " + self.id)

    def compute_layout(self):
        """Compute flow drawing path :return:"""
        # FIXME: calculer le chemin correctement ...
        nodes = self.diagram.get_all_nodes()

        flow_origin = nodes[self.origin]

        self.logger.warning(nodes.keys())
        flow_dest = nodes[self.dest]

        orig_x, orig_y, orig_offset = flow_origin.get_out_connector(self.proto)
        dest_x, dest_y, dest_offset = flow_dest.get_in_connector(self.proto)

        y = (dest_y - dest_offset) - (orig_y + orig_offset)
        total_y = (dest_y) - (orig_y)
        # 2 * INTERZONE_SPACE_Y \
        if y > (2 * ZONE_PADDING + NODES_GRID_Y) \
                or total_y < 0 \
                or (abs(dest_x - orig_x) > NODES_GRID_X / 2 and y < 0):
            # TODO: s'assurer que le routage ne passe pas au dessus d'un noeud
            left = (PATH_TRUNK_LEFT + (self.diagram.trunk_offset * PATH_OFFSET))
            x1 = left - orig_x
            x2 = dest_x - left
            self.diagram.trunk_offset += 1
            self.layout = "M%d %d v%d h%d v%d h%d v%d" % (
                orig_x, orig_y, orig_offset, x1, y, x2, dest_offset
            )
        elif (abs(dest_x - orig_x) < NODES_GRID_X / 2 and y < 0):
            # FIXME: avoid overflowing flows
            self.logger.warn("Direct Flow between %s and %s" % (flow_origin.name, flow_dest.name))
            self.layout = "M%d %d L%d %d" % (orig_x, orig_y, dest_x, dest_y)
            pass
        else:
            x = (dest_x - orig_x)
            y1 = orig_offset
            y2 = y
            self.layout = "M%d %d v%d h%d v%d v%d" % (
                orig_x, orig_y, y1, x, y2, dest_offset
            )

        self.logger.debug("Flow from '%s' to '%s' => path: '%s'" % (flow_origin.name, flow_dest.name, self.layout))
