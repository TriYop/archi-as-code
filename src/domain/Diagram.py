import logging
from datetime import date

import yaml

from constants import *
from domain.DepsZone import DepsZone
from domain.Flow import Flow
from domain.Zone import Zone
from domain.ZoneGroup import Zonegroup
from os import path

class Diagram(object):
    logger = logging.getLogger("Diagram")

    def __init__(self, source):
        # May be replace by any parser (IE json, XML, ASN.1, etc.)
        # to work with external webservices. This may even be parameterized
        """
        Args:
            source:
        """
        sats = yaml.safe_load(source)
        self.trunk_offset = 1
        self.in_connector_offset = 0
        self.out_connector_offset = 0
        if sats is not None:
            metas = sats['schema']['meta']
            self.application = metas['application']
            self.project = metas['project']
            self.confidentiality = metas['C']
            self.integrity = metas['I']
            self.availability = metas['D']
            self.traces = metas['T']
            self.icp = metas['ICP']
            self.metier = metas['metier']
            self.status = metas['status']
            self.drp = metas['DRP']
            self.rto = metas['RTO']
            self.spo = metas['RPO']
            self.technical_architect = metas['technical-architect']
            self.security_architect = metas['security-architect']
            self.date = str(date.today())
            self.referfence = "SATS-%s_%s-%s" % (self.application[:7], self.project[:7], self.status)

            self.groups = []

            zones = [Zone("Z%02d" % zone_id, zone, self) for zone_id, zone in
                     enumerate(sats['schema']['contents'], start=1)]
            deps = {"name": "Dépendances", "services": sats['schema']['dependencies']}
            zones.append(DepsZone(deps, self))

            gps = {}

            for zone in zones:
                if zone.type in ZONES_OPTIONS:
                    if zone.type not in gps:
                        gps[zone.type] = Zonegroup(zone.type)
                    gps[zone.type].append_zone(zone)
                else:
                    self.logger.fatal("Zone type '%s' is not valid for zone %s." % (zone.type, zone.id))

            self.groups = [gps[t] for t in gps]

            self.flows = [Flow("F%03d" % flow_id, flow, self) for flow_id, flow in
                          enumerate(sats['schema']['flows'], start=1)]

            self.scale = 1.

    def get_all_nodes(self):
        return {node.id: node for zone in self.allzones for node in zone.all_nodes}

    def compute_layout(self):
        self.logger.info("Starting diagram layout")
        for group in self.groups:
            group.layout_content()

        gpdict = {group.id: group for group in self.groups}
        self.allzones = [zone for gp in self.groups for zone in gp.zones]
        self.appzones = [zone for gp in self.groups for zone in gp.zones if zone.id != 'dependencies']

        max_x = 0
        max_y = 0
        for gpid in ['DEP', 'intranet', 'internet', 'internetF', 'intranetF', 'internetPA', 'internetP', 'intranetPA',
                     'intranetP', 'internetA', 'intranetA',
                     'internetD', 'intranetD', 'internetPAD', 'intranetPAD']:
            if gpid not in gpdict:
                self.logger.warning("Group '%s' is not a known group" % gpid)
                continue

            if ZONES_OPTIONS[gpid]['after-x'] == None:
                gpdict[gpid].left = DIAGRAM_MARGIN
            else:
                afterx = [zid for zid in ZONES_OPTIONS[gpid]['after-x'] if zid in gpdict]
                predecessor_x = [gpdict[prv].left + gpdict[prv].width for prv in afterx]
                predecessor_x.append(INTERZONE_SPACE_X)
                gpdict[gpid].left = INTERZONE_SPACE_X + max(predecessor_x)

            if ZONES_OPTIONS[gpid]['after-y'] == None:
                gpdict[gpid].top = INTERZONE_SPACE_Y
            else:
                aftery = ZONES_OPTIONS[gpid]['after-y']
                predecessor_y = [gpdict[prv].top + gpdict[prv].height for prv in aftery if prv in gpdict]
                predecessor_y.append(INTERZONE_SPACE_Y)
                gpdict[gpid].top = INTERZONE_SPACE_Y + max(predecessor_y)

            if gpdict[gpid].left + gpdict[gpid].width > max_x:
                max_x = gpdict[gpid].left + gpdict[gpid].width
            if gpdict[gpid].top + gpdict[gpid].height > max_y:
                max_y = gpdict[gpid].top + gpdict[gpid].height

        max_x += INTERZONE_SPACE_X
        max_y += INTERZONE_SPACE_Y

        self.scale = min(PAGE_WIDTH / float(max_x), PAGE_HEIGHT / float(max_y))
        self.width = int(max_x * self.scale)
        self.height = int(max_y * self.scale)

        for group in self.groups:
            for zone in group.zones:
                for node in zone.all_nodes:
                    node.offset_x = group.left + zone.left
                    node.offset_y = group.top + zone.top

        nodes = {node.id: node for group in self.groups for zone in group.zones for node in zone.services}
        nodes.update({node.id: node for group in self.groups for zone in group.zones for node in zone.nodes})

        # A Indexer sur le protocole et le node source
        nflows = len(self.flows)
        flown = 0

        for flow in self.flows:
            flown += 1
            flow.compute_layout()
