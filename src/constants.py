# Constants
from os.path import abspath, join, dirname

PAGE_WIDTH = 4200.
PAGE_HEIGHT = 2600.

DIAGRAM_MARGIN = 50
INTERZONE_SPACE_X = 450
INTERZONE_SPACE_Y = 280
ZONE_OFFSET_X = 120
ZONE_OFFSET_Y = 0
ZONE_PADDING = 60

NODES_GRID_X = 400
NODES_GRID_Y = 130
INTERNODE_SPACE_X = 80
INTERNODE_SPACE_Y = 180

TEMPLATES_PATH = abspath(join(dirname(__file__), "../resources/templates"))
PATH_TRUNK_LEFT = NODES_GRID_X + 2 * ZONE_OFFSET_X + DIAGRAM_MARGIN

PATH_OFFSET = 25

ZONES_OPTIONS = {
    'DEP': {
        'height': 0,
        'width': 1,
        'share': 'height',
        'after-x': None,
        'after-y': None
    },

    'intranet': {
        'height': 1,
        'width': 0,
        'share': 'width',
        'after-x': ['DEP'],
        'after-y': None
    },

    'internet': {
        'height': 1,
        'width': 0,
        'share': 'width',
        'after-x': ['DEP', 'intranet'],
        'after-y': None
    },

    'intranetF': {
        'height': 1,
        'width': 0,
        'share': 'width',
        'after-x': ['DEP'],
        'after-y': ['intranet', 'internet']
    },
    'internetF': {
        'height': 1,
        'width': 0,
        'share': 'width',
        'after-x': ['DEP', 'intranetF'],
        'after-y': ['intranet', 'internet']

    },

    'intranetP': {
        'height': 1,
        'width': 0,
        'share': 'width',
        'after-x': ['DEP'],
        'after-y': ['internetF', 'intranetF']
    },
    'internetP': {
        'height': 1,
        'width': 0,
        'share': 'width',
        'after-x': ['DEP', 'intranetP', 'internetPA'],
        'after-y': ['internetF']
    },

    'internetPA': {
        'height': 1,
        'width': 0,
        'share': 'width',
        'after-x': ['DEP'],
        'after-y': ['internetF', 'intranetF']
    },
    'intranetPA': {
        'height': 1,
        'width': 0,
        'share': 'width',
        'after-x': ['DEP', 'internetPA'],
        'after-y': ['internetF', 'intranetF']
    },

    'internetA': {
        'height': 1,
        'width': 0,
        'share': 'width',
        'after-x': ['DEP', 'internetPA', 'intranetPA'],
        'after-y': ['internetP', 'intranetP']
    },
    'intranetA': {
        'height': 1,
        'width': 0,
        'share': 'width',
        'after-x': ['DEP', 'internetPA', 'internetA', 'intranetPA'],
        'after-y': ['internetP', 'intranetP']
    },

    'internetD': {
        'height': 1,
        'width': 0,
        'share': 'width',
        'after-x': ['DEP'],
        'after-y': ['internetA', 'internetPA', 'intranetA', 'intranetPA']
    },
    'intranetD': {
        'height': 1,
        'width': 0,
        'share': 'width',
        'after-x': ['DEP', 'internetD'],
        'after-y': ['internetA', 'internetPA', 'intranetA', 'intranetPA']
    }

}

DEFAULT_TOPOLOGY = 'SaaS'

DEFAULT_ACTIVE = 'Actif'
