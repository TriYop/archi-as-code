#!/bin/zsh

export SCRIPT_DIR=$(readlink -f $0 | xargs -i dirname {})
export PYTHONPATH=$SCRIPT_DIR/src

mkdir -p reports

pylint -f json -r y src > reports/pylint.out.json
flake8 --format json src > reports/flake8.out.json
depcheck -f XML --enableExperimental -o reports -s .
bandit -r -o reports/bandit.out.json -f json src
pytest --cov=src --cov-report xml:reports/coverage.xml tests
sonar-scanner
